Adapter board for routing UART signals over SWD connector.
Credit to kylemanna for this approach.

![Front](swd-uart-splitter-front.png)
![Back](swd-uart-splitter-back.png)
