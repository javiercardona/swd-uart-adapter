EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:swd-uart-splitter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X05 P1
U 1 1 580FABD2
P 3500 3400
F 0 "P1" H 3500 3700 50  0000 C CNN
F 1 "CONN_02X05" H 3500 3100 50  0000 C CNN
F 2 "muchi_footprints:Header_SMD_10POS_0.05IN-Mirrored" H 3500 2200 60  0001 C CNN
F 3 "" H 3500 2200 60  0000 C CNN
F 4 "20021321-00010C4LF" H 3500 3400 60  0001 C CNN "MPN"
	1    3500 3400
	1    0    0    -1  
$EndComp
NoConn ~ 3750 3400
NoConn ~ 3750 3500
Text Notes 4650 2825 0    118  ~ 24
To Debugger
Text Notes 2950 2825 0    118  ~ 24
To Board
Text Label 3750 3200 0    60   ~ 0
SWD_DIO
Text Label 3750 3300 0    60   ~ 0
SWD_CLK
Text Label 2825 3200 0    60   ~ 0
3V3
Text Label 2825 3300 0    60   ~ 0
GND
Text Label 2825 3400 0    60   ~ 0
GND
Text Label 2750 3500 0    60   ~ 0
BOARD_RX
Text Label 2750 3600 0    60   ~ 0
BOARD_TX
Text Label 3875 3600 0    60   ~ 0
~RESET
$Comp
L CONN_02X05 P2
U 1 1 58106150
P 5300 3400
F 0 "P2" H 5300 3700 50  0000 C CNN
F 1 "CONN_02X05" H 5300 3100 50  0000 C CNN
F 2 "muchi_footprints:Header_SMD_10POS_0.05IN" H 5300 2200 60  0001 C CNN
F 3 "" H 5300 2200 60  0000 C CNN
F 4 "20021121-00010C4LF" H 5300 3400 60  0001 C CNN "MPN"
	1    5300 3400
	1    0    0    -1  
$EndComp
NoConn ~ 5550 3400
NoConn ~ 5550 3500
Text Label 4625 3200 0    60   ~ 0
3V3
Text Label 4625 3300 0    60   ~ 0
GND
Text Label 4625 3400 0    60   ~ 0
GND
Text Label 5675 3600 0    60   ~ 0
~RESET
Text Label 4650 4200 0    60   ~ 0
GND
Text Label 4600 4500 0    60   ~ 0
BOARD_RX
Text Label 4600 4600 0    60   ~ 0
BOARD_TX
Text Label 5550 3200 0    60   ~ 0
SWD_DIO
Text Label 5550 3300 0    60   ~ 0
SWD_CLK
Wire Wire Line
	3250 3400 2700 3400
Wire Wire Line
	3250 3300 2700 3300
Wire Wire Line
	3250 3200 2700 3200
Wire Wire Line
	2700 3600 3250 3600
Wire Wire Line
	2700 3500 3250 3500
Wire Wire Line
	3750 3200 4175 3200
Wire Wire Line
	3750 3300 4175 3300
Wire Wire Line
	3750 3600 4175 3600
Wire Wire Line
	5050 3400 4500 3400
Wire Wire Line
	5050 3300 4500 3300
Wire Wire Line
	5050 3200 4500 3200
Wire Wire Line
	5550 3600 5975 3600
Wire Wire Line
	4550 4200 5100 4200
Wire Wire Line
	5550 3200 5975 3200
Wire Wire Line
	5550 3300 5975 3300
Wire Notes Line
	6175 2900 4350 2900
Wire Notes Line
	4250 2900 4250 4575
Wire Notes Line
	4250 4575 2525 4575
Wire Notes Line
	2525 4575 2525 2900
Wire Notes Line
	2525 2900 4250 2900
Wire Wire Line
	5100 4500 4550 4500
Wire Wire Line
	5100 4600 4550 4600
NoConn ~ 5100 4300
NoConn ~ 5100 4400
Text Notes 4375 5075 0    60   ~ 0
pinout matches FTDI TTL/UART cables\nusing 5-pin header instead of 6\nto reduce board size
NoConn ~ 5050 3500
NoConn ~ 5050 3600
$Comp
L CONN_01X05 P3
U 1 1 5810DA0A
P 5300 4400
F 0 "P3" H 5300 4700 50  0000 C CNN
F 1 "CONN_01X05" V 5400 4400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" H 5300 4400 60  0001 C CNN
F 3 "" H 5300 4400 60  0000 C CNN
F 4 "MF-CON-2.54mm-01x05" H 5300 4400 60  0001 C CNN "Macrofab PN"
	1    5300 4400
	1    0    0    -1  
$EndComp
Wire Notes Line
	4350 2900 4350 3775
Wire Notes Line
	4350 3775 6175 3775
Wire Notes Line
	6175 3775 6175 2900
Wire Notes Line
	4350 5100 6175 5100
Text Notes 4675 4000 0    118  ~ 24
To TTL/UART
Wire Notes Line
	4350 5100 4350 4050
Wire Notes Line
	4350 4050 6175 4050
Wire Notes Line
	6175 4050 6175 5100
$EndSCHEMATC
